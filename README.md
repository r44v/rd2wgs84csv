# README #

An simple script to convert RD(Rijksdriehoek) coordinates in a csv file (column X and Y) to WGS84/GPS coordinates and adding those a duplicate csv file with extra columns longitude and latitude

---

[Credit to https://thomasv.nl for the converter class](https://thomasv.nl/2014/03/rd-naar-gps/)