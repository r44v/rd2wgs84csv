import csv
import sys
from RDWGSConverter import RDWGSConverter


def convert(x, y):
    coords = [x, y]
    conv = RDWGSConverter()
    wgsCoords = conv.fromRdToWgs( coords )
    newCoords = conv.fromWgsToRd( wgsCoords )
    # Laat de coordinaten voor conversie en de coordinaten na dubbele conversie op het scherm zien
    # om te bewijzen dat het een benadering betreft. Tot slot worden ook nog de WGS coordinaten weergegeven.
    return (wgsCoords)    


def convert_controle(x, y):
    coords = [x, y]
    conv = RDWGSConverter()
    wgsCoords = conv.fromRdToWgs( coords )
    newCoords = conv.fromWgsToRd( wgsCoords )
    # Laat de coordinaten voor conversie en de coordinaten na dubbele conversie op het scherm zien
    # om te bewijzen dat het een benadering betreft. Tot slot worden ook nog de WGS coordinaten weergegeven.
    return [coords, wgsCoords, newCoords]


def self_dict(list):
    result = {}
    for item in list:
        pair = {item: item}
        result.update(pair)
    return result


args = sys.argv[1:]
if len(args) != 1:
    print 'usage: convert.py <inputfile>'
    sys.exit(2)

input_file_name = args[0]

output_file_name = "result.csv"

# Prepare input
input_file = open(input_file_name)
reader = csv.DictReader(input_file)

# check for title row
if reader.fieldnames is None:
    print "Input file invalid (no fieldnames)"
    sys.exit(2)

# check for longitude and latitude rows
if 'longitude' in reader.fieldnames or 'latitude' in reader.fieldnames:
    print 'inputfile already contains longitude and/or latitude columns '
    sys.exit(1)

# Create title row
title_row = []
title_row.extend(reader.fieldnames)
title_row.append('longitude')
title_row.append('latitude')

# Prepare output
output_file = open(output_file_name, 'wb')
writer = csv.DictWriter(f=output_file, fieldnames=title_row)
writer.writerow(self_dict(title_row))

print title_row

for row in reader:
    res = convert(float(row['X']), float(row['Y']))
    res = {'longitude': res[0], 'latitude': res[1]}
    row.update(res)
    writer.writerow(row)